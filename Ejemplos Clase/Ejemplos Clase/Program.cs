﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplos_Clase
{
    class Program
    {
        /// <summary>
        /// Este es el metodo main (principal de la clase Program), en este metodo ocurre lo que ustedes decidan que ocurra, desde lectura de datos
        /// hasta invocacion de funciones que existan en otras clases, asi como la creacion de objetos y lo que ustedes imaginen!
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Console.WriteLine("Hola Jovenes, Un Placer Saludarles :D, Aqui En Este Programa Tienen Ejemplos De Como Usar [if], [else if], \n" +
                "[for], [switch/case], En Diferentes Situaciones! \n" +
                "A Partir De Hoy, Asi Quiero Sus Practicas, Por Fas! \n" +
                "Se Que Ustedes Pueden :D ");
            int opcionE = 0;
            Console.WriteLine("Estimado Usuario Para Poder Testear Este Software Debe Ingresar Un Numero Comprendido Entre El 1 y El 5, Gracias \n");
            opcionE = int.Parse(Console.ReadLine());
            switch (opcionE)
            {
                case 1:
                    Console.WriteLine("****************- Primera Seccion -********************************");
                    Console.WriteLine("Este Programa Invierte La Posicion De Tres Numeros");
                    int valor, invert;
                    Console.WriteLine("Ingrese Numero De Tres Cifras : \n");
                    valor = int.Parse(Console.ReadLine());
                    invert = herramientas.inverteNum(valor);
                    Console.WriteLine("El Numero Invertido Es: \n" + invert);
                    break;
                case 2:
                    Console.WriteLine("****************- Segunda Seccion -********************************");
                    byte canB, canH, canP;
                    float totalCuenta;
                    Console.WriteLine("Este Programa Emula Ser La Caja De Un Establecimiento De Anvurguesas :P");
                    Console.Write("Cuantas anvorguesas te atascaste : \n");
                    canH = byte.Parse(Console.ReadLine());
                    Console.Write("Cuantas papitas te empacaste : \n");
                    canP = byte.Parse(Console.ReadLine());
                    Console.Write("Cuantas litros de soda te sampaste : \n");
                    canB = byte.Parse(Console.ReadLine());
                    totalCuenta = herramientas.vamosPorAnvorguesas(canH, canP, canB);
                    Console.WriteLine("Su cerdiada les va costar: \n" + totalCuenta);
                    break;
                case 3:
                    Console.WriteLine("****************- Tercera Seccion -********************************");
                    Console.WriteLine("Este Programa Hace Uso De La Libreria Math");
                    byte opcion;
                    int numF;
                    long resultado;
                    Console.WriteLine("Inserta un numero : \n");
                    numF = int.Parse(Console.ReadLine());
                    Console.WriteLine("Tienes 9 opciones las cuales son las siguientes: \n" +
                        "1) Valor Absoluto De Un Numero \n" +
                        "2) Cuadrado De Un Numero \n" +
                        "3) Raiz Cuadrada De Un Numero \n" +
                        "4) Seno De Un Numero \n" +
                        "5) Coseno De Un Numero \n" +
                        "6) Min De Un Numero \n" +
                        "7) Max De Un Numero \n" +
                        "8) Parte Entera De Un Numero \n" +
                        "6) Redondeo De Un Numero \n");
                    opcion = byte.Parse(Console.ReadLine());
                    resultado = herramientas.funcionesDeLibreria(numF, opcion);
                    Console.WriteLine("El Resultado De La Operacion Elegida Es: \n" + resultado);
                    break;
                case 4:
                    Console.WriteLine("****************- Cuarta Seccion -********************************");
                    Console.WriteLine("Este Programa Ejemplifica Switch Case, Determinando Tu Estado Civil");
                    char eCivil;
                    Console.Write("Este Programa Determina Tu Estado Civil: \n");
                    Console.Write("Introduce Cualquiera De Las Siguientes Opciones C,S,V,D: \n");
                    eCivil = char.Parse(Console.ReadLine());
                    herramientas.soyOnoSoyGanado(eCivil);
                    break;
                case 5:
                    Console.WriteLine("****************- Quinta Seccion -********************************");
                    Console.WriteLine("Este Programa Emula Ser Un Software Administrativo Que Determine El Presupuesto Para Un Negocio");
                    double cantP, tPresupuesto;
                    byte can, opt;
                    Console.Write("Cuantos Calculos Necesita: \n");
                    can = byte.Parse(Console.ReadLine());
                    Console.Write("Con Cuanto Presupuesto Cuenta Actualmente: \n");
                    cantP = double.Parse(Console.ReadLine());
                    Console.WriteLine(" Nuestro software Tiene La Capacidad De Realizar El Calculo Para Tres Areas: \n" +
                        "Las Cuales Ya Cuentan Con Un Porcetanje Predifinido; \n" +
                        "Sus Opciones Pueden Ser Del 1 Al 3 \n");
                    Console.Write("Digite El Area Con La Que Se Va Trabajar: \n");
                    opt = byte.Parse(Console.ReadLine());
                    tPresupuesto = herramientas.sacandoPresupuestos(can, cantP, opt);
                    Console.WriteLine(" El Area Selecionada: " + opt + " Recibe: " + tPresupuesto);
                    break;
                default:
                    Console.WriteLine("Esta Opcion No Existe Compitas :P \n");
                    break;
            }
            Console.WriteLine("Pulse una Tecla Para Salir: \n");
            Console.ReadKey();
        }

    }
}
