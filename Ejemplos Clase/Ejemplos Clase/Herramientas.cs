﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplos_Clase
{
    class herramientas
    {
        /// <summary>
        /// Esta funcion me permite invertir el orden de un numero de 3 cifras realizando operaciones basicas y modulo de un numero
        /// </summary>
        /// <param name="x"> El parametro x es el numero que se le solicita al usurio y con el  que vamos a trabajar </param>
        /// <returns> Res es numero resultante ya invertido dentro de esta funcion </returns>
        public static int inverteNum(int x)
        {
            int dec, uni, cen, res = 0; ;
            cen = x / 100;
            x = x % 100;
            dec = x / 10;
            uni = x % 10;
            res = (uni * 100) + (dec * 10) + cen;
            return res;
        }

        /// <summary>
        /// Este metodo emula un pequenio sistemita que se usa cuando van a comer anvorguesas jovenes, realiza la operacion de calcular la cuenta
        /// de cuanto han consumido en el establecimiento
        /// </summary>
        /// <param name="cantHam"> Este parametro recibibe la cantidad anvorguesas que se comieron tu y tus compas </param>
        /// <param name="cantPapas"> Este parametro recibibe la cantidad de papas que se comieron tu y tus compas </param>
        /// <param name="cantRefresco"> Este parametro recibibe la cantidad de litros que se tomaron tu y tus compitas </param>
        /// <returns></returns>

        public static float vamosPorAnvorguesas(byte cantHam, byte cantPapas, byte cantRefresco)
        {
            float totalCuenta = 0;
            const float precioB = 0.8f;
            const float precioH = 2;
            const float precioP = 1.2f;
            totalCuenta = (cantHam * precioH) + (cantPapas * cantPapas) + (cantRefresco * precioB);
            return totalCuenta;
        }

        /// <summary>
        /// Esta es una funcion que nos permite hacer el calculo de alguna funciones matematicas
        /// haciendo uso de la libria Math
        /// </summary>
        /// <param name="elemento"> El valor que hemos leido por teclado para obtener algun resultado</param>
        /// <param name="op"> Es la operacion matematica que se desea aplicar</param>
        /// <returns></returns>
        public static long funcionesDeLibreria(int elemento, byte op)
        {
            long res = 0;
            if (op == 1)
            {
                res = Math.Abs(elemento);
            }
            else if (op == 2)
            {
                res = (long)(Math.Pow(elemento, 2));
            }
            else if (op == 3)
            {
                res = (long)Math.Sqrt(elemento);
            }
            else if (op == 4)
            {
                res = (long)(Math.Sin(elemento));
            }
            else if (op == 5)
            {
                res = (long)(Math.Cos(elemento));
            }
            else if (op == 6)
            {
                res = (long)(Math.Min(elemento, 50));
            }
            else if (op == 7)
            {
                res = (long)(Math.Max(elemento, 50));
            }
            else if (op == 8)
            {
                res = (long)(Math.Truncate(elemento * 0.000001));
            }
            else if (op == 9)
            {
                res = (long)(Math.Round(elemento * 0.00000000000000000001));
            }
            return res;
        }

        /// <summary>
        /// Este metodo solo es para ejemplificar el uso de switch case
        /// </summary>
        /// <param name="estado"> La opcion para realizar la validacion </param>
        public static void soyOnoSoyGanado(char estado)
        {
            switch (estado)
            {
                case 'C':
                    ; Console.WriteLine("Estas Fuera Del Juego Compa \n");
                    break;
                case 'S':
                    ; Console.WriteLine("Perteneces A La Sociedad Libre De Ganado \n");
                    break;
                case 'V':
                    ; Console.WriteLine("Tu Nivel Se Encuentra Mas Alto En La Sociedad Ganadera De Carne \n");
                    break;
                case 'D':
                    ; Console.WriteLine("Ya Te Chupo El Diablo, Ya Estan Amañados :P \n");
                    break;
                default:
                    Console.WriteLine("No Existe, No Hay, Somos Seres Evolucionaos ;) \n");
                    break;
            }
        }

        /// <summary>
        /// Este metodo emula el comportamiento de un software administrativo para determinar el presupuesto de alguna entidad
        /// </summary>
        /// <param name="numOpe"> Cuantas veces necesitamos el proceso </param>
        /// <param name="totalPresupuesto"> Total maximo de presupuesto</param>
        /// <param name="area"> Area de la cual se desea conocer el porcentaje </param>
        /// <returns></returns>
        public static double sacandoPresupuestos(byte numOpe, double totalPresupuesto, byte area)
        {
            double totalGastos = 0, porcentaje = 0;
            for (int i = 0; i <= numOpe; i++)
            {
                switch (numOpe)
                {
                    case 1:
                        porcentaje = 50;
                        break;
                    case 2:
                        porcentaje = 40;
                        break;
                    case 3:
                        porcentaje = 30;
                        break;
                    default:

                        break;
                }
                totalGastos = (porcentaje * totalPresupuesto) / 100;
            }
            return totalGastos;
        }
    }

}
