﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace do_Sostenido_Arreglos
{
    class Program
    {
        static void Main(string[] args)
        {
            ///Ejercicios:
            /// 1.-  Que rellene un array con los 100 primeros números enteros y los muestre en pantalla en orden ascendente.
            /// 2.- Que rellene un array con los 100 primeros números enteros y los muestre en pantalla en orden descendente.
            /// 3.- Que lea 10 números por teclado, los almacene en un array y muestre la suma, resta, multiplicación y división de todos.
            ///


            byte A;
            Console.WriteLine("Este programa resuelve problemas con opciones del 1 al 3, selecciona el problema que te gustaría realizar: \n");
            Console.WriteLine("1.- Array con los 100 primeros números en orden ascendente.");
            Console.WriteLine("2.- Array con los 100 primeros números en orden descendente.");
            Console.WriteLine("3.- Leer 10 números, almacenarlos en un array y muestre la suma, resta, multiplicación de todos.");
            A = byte.Parse(Console.ReadLine());

            switch (A)
            {
                case 1:
                    Herramientas.ordenAscendente();
                    break;
                case 2:
                    Herramientas.ordenDescendente();
                    break;
            }




            Console.WriteLine("Fin del programa...");
            Console.WriteLine("Presiona 'Enter' para salir...");
            Console.ReadKey();
        }
    }
}
