﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejerciciosConMatrices15_11_2019
{
    class Ejercicios
    {
        public static void Ejercicio2()
        {
            int N, i = 0, j = 0, MI = 0, FI = 0;

            Console.Write("Ingresa el tamaño que tendrá la matriz (impar):");
            N = int.Parse(Console.ReadLine());
            N = (N % 2 == 0 ? N + 1 : N);
            string[,] MAT = new string[N + 1, N + 1];

            for (i = 1; i <= N; i++)
            {
                for (j = 1; j <= N; j++)
                {
                    MAT[i, j] = " ";
                }
                MI = N / 2 + 1;
                for (i = 1; i <= N; i++)
                {
                    MAT[i, 1] = "+";
                    MAT[MI, i] = "+";
                    MAT[1, i] = "+";
                }
                FI = MI;
                for (i = 1; i <= MI; i++)
                {
                    MAT[i, N] = "+";
                    MAT[FI, FI] = "+";
                    FI = FI + 1;
                }
                for (i = 1; i <= N; i++)
                {
                    for (j = 1; j <= N; j++)
                    {
                        Console.SetCursorPosition(j, i + 1);
                        Console.Write(MAT[i, j]);
                    }
                }
            }
        }

        public static void Ejercicio3()
        {
            int N, i = 0, j = 0, nMayor = 0, nMenor = 0;
            Console.Write("Ingresa el tamaño de la matriz:");
            N = int.Parse(Console.ReadLine());
            Random rnd = new Random();
            int[,] MAT = new int[N + 1, N + 1];
            for (i = 1; i <= N; i++)
            {
                for (j = 1; j <= N; j++)
                {
                    MAT[i, j] = rnd.Next(0, 99);
                    Console.SetCursorPosition(j * 4, i + 1);
                    Console.Write(MAT[i, j]);
                }
            }
            nMayor = MAT[1, 1];
            nMenor = MAT[1, 1];
            for (i = 1; i <= N; i++)
            {
                for (j = 1; j <= N; j++)
                {
                    if ((MAT[i, j] > nMayor))
                        nMayor = MAT[i, j];
                    if ((MAT[i, j] < nMenor))
                        nMenor = MAT[i, j];
                }
            }
            Console.WriteLine("\n");
            Console.WriteLine("EL número mayor es: " + nMayor);
            Console.WriteLine("El número menor es: " + nMenor);
        }

        public static void Ejercicio4()
        {
            int N, i = 0, j = 0, I = 0, J = 0, AUX = 0;
            Console.WriteLine("Este programa ordena la matriz de menor a mayor.");
            Console.Write("Ingresa el tamaño de la matriz: ");
            N = int.Parse(Console.ReadLine());
            Random rnd = new Random();
            int[,] MAT = new int[N + 1, N + 1];
            for (i = 1; i <= N; i++)
            {
                for (j = 1; j <= N; j++)
                {
                    MAT[i, j] = rnd.Next(0, 100);
                    Console.SetCursorPosition(j * 4, i + 1);
                    Console.Write(MAT[i, j]);
                }
            }
            for (i = 1; i <= N; i++)
            {
            for (j = 1; j <= N; j++)
                {
                    for (I = 1; I <= N; I++)
                    {
                        for (J = 1; J <= N; J++)
                        {
                            if ((MAT[i, j] < MAT[I, J]))
                            {
                                AUX = MAT[i, j];
                                MAT[i, j] = MAT[I, J];
                                MAT[I, J] = AUX;
                            }
                        }
                    }
                }
            }
            for (i = 1; i <= N; i++)
            {
                for (j = 1; j <= N; j++)
                {
                    Console.SetCursorPosition(j * 4, i + 10);
                    Console.Write(MAT[i, j]);
                }
            }
        }

        public static void Ejercicio5()
        {
            int N, i = 0, j = 0, nMenor = 0;
            Console.Write("Ingresa el tamaño de la matriz: ");
            N = int.Parse(Console.ReadLine());
            int[,] MAT = new int[100, 100];
            int[] VEC = new int[N + 1];
            Random rnd = new Random();
            for (i = 1; i <= N; i++)
            {
                for (j = 1; j <= N; j++)
                {
                    MAT[i, j] = rnd.Next(0, 100);
                    Console.SetCursorPosition(j * 4, i + 1);
                    Console.Write(MAT[i, j]);
                }
            }
            for (j = 1; j <= N; j++)
            {
                nMenor = MAT[1, j];
                for (i = 1; i <= N; i++)
                {
                    if ((MAT[i, j] < nMenor))
                        nMenor = MAT[i, j];
                }
                VEC[j] = nMenor;
            }
            for (j = 1; j <= N; j++)
            {
                Console.SetCursorPosition(j * 4, 15);
                Console.Write(VEC[j]);
            }
        }

        public static void Ejercicio6()
        {
            int N, i = 0, j = 0, nMayor = 0;
            Console.Write("Ingresa el tamaño de la matriz: ");
            N = int.Parse(Console.ReadLine());
            int[,] MAT = new int[100, 100];
            int[] VEC = new int[N + 1];
            Random rnd = new Random();
            for (i = 1; i <= N; i++)
            {
                for (j = 1; j <= N; j++)
                {
                    MAT[i, j] = rnd.Next(0, 100);
                    Console.SetCursorPosition(j * 4, i + 1);
                    Console.Write(MAT[i, j]);
                }
            }
            for (i = 1; i <= N; i++)
            {
                nMayor = MAT[i, 1];
            for (j = 1; j <= N; j++)
                {
                    if (MAT[i, j] > nMayor)
                        nMayor = MAT[i, j];
                }
                VEC[i] = nMayor;
            }
            for (i = 1; i <= N; i++)
            {
                Console.SetCursorPosition(50, i + 1);
                Console.Write(VEC[i]);
            }
        }
    }
}