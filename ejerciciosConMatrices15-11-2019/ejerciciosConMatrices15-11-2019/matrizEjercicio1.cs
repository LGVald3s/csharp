﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejerciciosConMatrices15_11_2019
{
    class matrizEjercicio1
    {
        private string[] empleados;
        private int[,] sueldos;
        private int[] sueldostot;

        public void Cargar()
        {
            empleados = new String[4];
            sueldos = new int[4, 3];
            for (int i = 0; i < empleados.Length; i++)
            {
                Console.Write("Ingrese el nombre del empleado " + (i + 1) + ": ");
                empleados[i] = Console.ReadLine();
                for (int j = 0; j < sueldos.GetLength(1); j++)
                {
                    Console.Write("Ingrese sueldo " + (j + 1) + ": ");
                    string sueldo;
                    sueldo = Console.ReadLine();
                    sueldos[i, j] = int.Parse(sueldo);
                }
            }
        }

        public void CalcularSumaSueldos()
        {
            sueldostot = new int[4];
            for (int i = 0; i < sueldos.GetLength(0); i++)
            {
                int suma = 0;
                for (int j = 0; j < sueldos.GetLength(1); j++)
                {
                    suma = suma + sueldos[i, j];
                }
                sueldostot[i] = suma;
            }
        }

        public void ImprimirTotalPagado()
        {
            Console.WriteLine("Total de sueldos pagados a cada empleado.");
            for (int i = 0; i < sueldostot.Length; i++)
            {
                Console.WriteLine("El empleado " + empleados[i] + " ganó: $" + sueldostot[i]);
            }
        }

        public void EmpleadoMayorSueldo()
        {
            int sMayor = sueldostot[0];
            string nomEmpleado = empleados[0];
            for (int i = 0; i < sueldostot.Length; i++)
            {
                if (sueldostot[i] > sMayor)
                {
                    sMayor = sueldostot[i];
                    nomEmpleado = empleados[i];
                }
            }
            Console.WriteLine("El empleado con mayor sueldo es " + nomEmpleado + ", que tiene un sueldo de: $" + sMayor);
        }
    }
}
