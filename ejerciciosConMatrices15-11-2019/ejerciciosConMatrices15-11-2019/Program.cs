﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejerciciosConMatrices15_11_2019
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Este programa contiene ejercicios con matrices, contando con opciones del 1 al 6, selecciona el ejercicio que desees.");
            Console.WriteLine("1.- Ejercicio 1");
            Console.WriteLine("2.- Fomrar la letra 'R' en una matriz de NxN");
            Console.WriteLine("3.- El número mayor y el número menor en una matriz de NxN");
            Console.WriteLine("4.- Ordenamiento de una matriz de NxN");
            Console.WriteLine("5.- Los números menores de cada columna de una matriz de NxN en un vector");
            Console.WriteLine("6.- Los números mayores de cada fila de una matriz de NxN en un vector");
            byte N = byte.Parse(Console.ReadLine());
            Console.Clear();
            switch (N)
            {
                case 1:
                    matrizEjercicio1 ma = new matrizEjercicio1();
                    ma.Cargar();
                    ma.CalcularSumaSueldos();
                    Console.WriteLine("\n");
                    ma.ImprimirTotalPagado();
                    Console.WriteLine("\n");
                    ma.EmpleadoMayorSueldo();
                    break;

                case 2:
                    Ejercicios.Ejercicio2();
                    break;

                case 3:
                    Ejercicios.Ejercicio3();
                    break;

                case 4:
                    Ejercicios.Ejercicio4();
                    break;

                case 5:
                    Ejercicios.Ejercicio5();
                    break;

                case 6:
                    Ejercicios.Ejercicio6();
                    break;
            }


            Console.WriteLine("\n\n\n");
            Console.WriteLine("Fin del programa...");
            Console.WriteLine("Presiona 'Enter' para salir...");
            Console.ReadKey();
        }
    }
}
